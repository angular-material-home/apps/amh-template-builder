/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amh-template-builder')
//
.run(function() {
//	/*
//	 * Register a tips 
//	 */
//	$wbUi
//
////	.newTemplate({
////		name : 'Page not found',
////		description: 'Set a not found page',
////		thumbnail : 'resources/images/page-not-found.svg',
////		templateUrl: 'resources/templates/page-not-found.json',
////		language: 'en',
////		priority: 100
////	})
//	.newTemplate({
//		name : 'Portfolio',
//		description: 'Portfolio template',
//		thumbnail : 'resources/templates/portfolio/thumbnail.png',
//		templateUrl: 'resources/templates/portfolio/main.json',
//		language: 'fa',
//		priority: 1000
//	})
//	.newTemplate({
//		name : 'Classic',
//		description: 'Classic page',
//		thumbnail : 'resources/templates/classic/thumbnail.png',
//		templateUrl: 'resources/templates/classic/main.json',
//		language: 'en',
//		priority: 100
//	});
});

