/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

// Test controller
angular.module('amh-template-builder')

.controller('TemplateCtrl', function($scope, $actions, $cms, $http, $routeParams, $rootScope,
		$widget, $mdSidenav, $navigator, $page, $wbUtil, $translate, uuid4, FileSaver, $wbmodel, $q,
		WidgetLocatorManager) {
	
	
	
	/****************************************************************************************
	 * Abstract editor
	 * 
	 * NOTE: the AMH editor can use this part as controller of the WB content
	 ****************************************************************************************/
	var MIME_WB = 'application/weburger+json';
	var stateMachine = null;
//	var keyCommandHandler = new AmhKeyCommandHandler();
	var widgetLocatorManager = new WidgetLocatorManager({
		selectionEnable: true,
		selectionPath: true,
		cursorEnable: true,
		cursorPath: false,
		actionEnable: false
	});

	
	this.rootWidget = null;
	this.selectedWidgets = [];
	this.type = 'weburger';
	
	
	
	/****************************************************************************************
	 * General management
	 ****************************************************************************************/
	this.destroy = function(){
		widgetLocatorManager.destroy();
	};
	
	this.load = function(){
		var ctrl = this;
		$q.when(ctrl.loadModelMeta())
		.then(function(modelMeta){
			ctrl.setModelMeta(modelMeta);
			// TODO: maso, 2018: Check type of the editor
			return ctrl.loadModel();
		})
		.then(function(model){
			ctrl.setModel(model);
			ctrl.loadPageInfo();
		},function(/*error*/){
			// TODO:
		});
	};
	
	this.isEditorLoaded = function(){
		return this._editorLoaded;
	};
	
	this.setEditorLoaded = function(editorLoaded){
		this._editorLoaded = editorLoaded;
	};
	
	this.getEditMode = function(){
		return this._editable;
	};
	
	this.isEditMode = function(){
		return this._editable;
	};
	
	this.setEditMode = function(editMode) {
		this._editable = editMode;
		// load editor
		if(editMode) {
			this.setEditorLoaded(true);
		}
		// load locators
		widgetLocatorManager.setVisible(editMode);
		if (!widgetLocatorManager.isEnable()) {
			widgetLocatorManager.setEnable(true);
		}
	};

	// Show preview of designed page
	this.showPreview = function (/*type*/) {
		// XXX: maso, 2018: just remove locators 
	};
	
	/****************************************************************************************
	 * Model management
	 ****************************************************************************************/

	/**
	 * Load page description
	 * 
	 * Page SEO is loaded with $page service
	 * 
	 * @memberof AbstractEditorCtrl
	 */
	this.loadPageInfo = function() {
		var model = this.getModel();
		// Load SEO and page
		$page.setTitle(model.label) //
			.setKeywords(model.keywords) //
			.setDescription(model.description);
	};

	/**
	 * Downloads content value and stores in a file
	 * 
	 * @memberof AmhContentCtrl
	 * @return {promiss} to save content value
	 */
	this.downloadModel = function() {
		var ctrl = this;
		$navigator.openDialog({
			templateUrl: 'views/dialogs/amh-content-download-options.html',
			config: {
				options: {
					fileName: ctrl.getModel().label
				}
			}
		})
		.then(function (options) {
			// TODO: change the state to working
			var model = ctrl.getModel();
			var job = null;
			if (options.embeddedImage) {
				model = _.cloneDeep(model);
				job = $wbmodel.embedImagesDeep(model);
			}
			$q.when(job)
			.then(function () {
				// save the model
				var dataString = JSON.stringify(model);
				var data = new Blob([dataString], {
					type: MIME_WB
				});
				return FileSaver.saveAs(data, (options.fileName || $scope.content.name) + '.wb');
			});
		});
	};

	/**
	 * Upload a file as the content of the page
	 * 
	 * @memberof AmhContentCtrl
	 * @return {promiss} to save content value
	 */
	this.uploadModel = function(/* modelOption */) {
		// TODO: maso, 2019: handle upload model from modelOption.url
//		$http.get(template.templateUrl)//
//		.then(function (response) {
//			// TODO: Maso, 2018: $wbUtil must delete in next version. Here
//			// it comes for compatibility to previous versions.
//			$scope.model = $wbUtil.clean(response.data);
//		});//
		// upload from file with dialog
		var ctrl = this;
		var fileInput = document.createElement('input');
		fileInput.type = 'file';
		fileInput.style.display = 'none';
		fileInput.onchange = function (event) {
			var reader = new FileReader();
			reader.onload = function (event) {
				var model = JSON.parse(event.target.result);
				model = $wbUtil.clean(model);
				ctrl.setModel(model);
				$scope.$apply();
			};
			reader.readAsText(event.target.files[0]);
		};
		document.body.appendChild(fileInput);
		// click Elem (fileInput)
		var eventMouse = document.createEvent('MouseEvents');
		eventMouse.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		fileInput.dispatchEvent(eventMouse);
	};
	
	this.setModel = function(model) {
		this.model = model;
	};
	
	this.getModel = function(){
		return this.model;
	};
	

	/*********************************************************************
	 * sidenaves
	 *********************************************************************/
	// used for toggling sidenavs in editing mode of content.
	var oldComponentId = '';

	/**
	 * Check if sidenave is open
	 * 
	 * @memberof AmhContentCtrl
	 * @params {string} id of the sidenav
	 */
	this.isSidenavOpen = function(id) {
		return $mdSidenav(id).isOpen();
	};

	/**
	 * Toggles the sidenav with the given id
	 * 
	 * @memberof AmhContentCtrl
	 * @params {string} componentId of the sidenav
	 */
	this.toggleSidenav = function(componentId) {
		if (componentId === oldComponentId) {
			$mdSidenav(componentId).toggle();
		} else if ($mdSidenav(oldComponentId).isOpen()) {
			$mdSidenav(oldComponentId).toggle();
			$mdSidenav(componentId).toggle();
			oldComponentId = componentId;
		} else {
			$mdSidenav(componentId).toggle();
			oldComponentId = componentId;
		}
	};


	// Open general help in right sidenav.
	this.openHelp = function () {
		// TODO: maso, 2018: replace with help service
		$rootScope.showHelp = !$rootScope.showHelp;
	};
	
	
	/*********************************************************************
	 * Widgets
	 *********************************************************************/
	// Load setting of selected widget
	this.handleWidgetEvent = function ($event) {
		var widgets = $event.widgets;
		// TODO: maso, 2019: check the event
		this.setSelectedWidgets(widgets);
	};

	this.setSelectedWidgets = function(widgets){
		ctrl.selectedWidgets = widgets;
		widgetLocatorManager.setSelectedWidgets(widgets);
		if(widgets.length && !ctrl.hasRootWidget()) {
			var widget = widgets[0];
			if (widget.isRoot()) {
				ctrl.setRootWidget(widget);
			}
		}
	};
	
	this.hasRootWidget = function(){};
	this.setRootWidget = function(/*widget*/){};
	this.getRootWidget = function(){};
	





	// -----------------------------------------------------------//
	// State handling
	// -----------------------------------------------------------//
	var ctrl = this;
	/*
	 * Manages the state of the controller
	 */
	stateMachine = new machina.Fsm({
		initialize: function (/* options */) {
			ctrl.state = 'waiting';
		},
		namespace: 'amh.content',
		initialState: 'waiting',
		states: {
			waiting: {
				appReady: 'loading'
			},
			loading: {
				_onEnter: function () {
					ctrl.load();
				},
				valueLoaded: 'clean',
				error404: 'E404',
				error5XX: 'E5XX'
			},
			saving: {
				saved: 'clean',
				error404: 'E404',
				error5XX: 'E5XX',
				changed: 'derty'
			},
			clean: {
				_onEnter: function () {
					ctrl.loadPageDescription($scope.model);
				},
				changed: 'derty'
			},
			derty: {
				saving: 'saving'
			},
			E404: {
				_onExit: function () {
					$scope.error = null;
				},
				saved: function () {
					this.transition('clean');
				}
			},
			E5XX: {},
			EU: {}
		},

		/*
		 * Meta date of the content id loaded
		 */
		metaLoaded: function () {
			this.handle('metaLoaded');
		},
		valueLoaded: function () {
			this.handle('valueLoaded');
		},
		appReady: function () {
			this.handle('appReady');
		},
		saved: function () {
			this.handle('saved');
		},
		saving: function () {
			this.handle('saving');
		},
		modelChanged: function () {
			this.handle('changed');
		},
		error: function (error) {
			switch (error.status) {
			case 404:
				this.handle('error404');
				break;
			case 500:
				this.handle('error5XX');
				break;
			default:
				this.handle('errorUnknown');
			}
		}
	});


	/*
	 * Lesson to the state machine transitions
	 */
	stateMachine.on('transition', function () {
		ctrl.state = stateMachine.state;
	});
	
	this.getState = function(){
		return this.state;
	};
	
	this.setState = function(state){
		this.state = state;
	};


	$scope.$on('$locationChangeStart', function (event, nextRoute/*, currentRoute*/) {
		if ($scope.state === 'derty') {
			event.preventDefault();
			return confirm('Leave page without saving changes?')
			.then(function () {
				// TODO: maso, 2018: replace with $page serveice
				$navigator.openpage(nextRoute);
			});
		}
	});

	/*
	 * Content name TODO: maso, 2018: call machine state changes
	 */
//	$scope.$on('$translateChangeSuccess', _loadContent);
	$scope.$watch('__app.state', function (status) {
		if (status && status.startsWith('ready')) {
			stateMachine.appReady();
		}
	});

	$scope.$on('$destroy', function () {
		ctrl.destroy();
	});
	
	// Loads widgets
	$q.when($widget.getWidgets())
	.then(function(list){
		ctrl.widgets = list.items;
	});
	
	// -----------------------------------------------------------//
	// Actions
	// -----------------------------------------------------------//

	/*
	 * Add scope menus
	 */
	$actions.newAction({// edit menu
		id: 'amh.scope.edit',
		priority: 15,
		icon: 'edit',
		title: 'Edit',
		description: 'Edit the page',
		action: function () {
			var isEditable = ctrl.getEditMode();
			ctrl.setEditMode(!isEditable);
		},
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	}); //
//	$actions.newAction({// create new menu
//		id: 'amh.scope.create',
//		priority: 15,
//		icon: 'add_box',
//		title: 'Create',
//		description: 'Create new page',
//		visible: _canCreate,
//		action: createContent,
//		groups: ['amh.owner-toolbar.scope'],
//		scope: $scope
//	}); //
//	$actions.newAction({// clone new menu
//		id: 'amh.scope.new',
//		priority: 15,
//		icon: 'content_copy',
//		title: 'Clone',
//		description: 'Clone page',
//		visible: _canEdit,
//		action: cloneContent,
//		groups: ['amh.owner-toolbar.scope'],
//		scope: $scope
//	}); //
//	$actions.newAction({// save menu
//		id: 'amh.scope.save',
//		priority: 10,
//		icon: 'save',
//		title: 'Save',
//		description: 'Save page',
//		visible: _canEdit,
//		action: saveModel,
//		groups: ['amh.owner-toolbar.scope'],
//		scope: $scope
//	});
//	$actions.newAction({// delete menu
//		id: 'amh.scope.delete',
//		priority: 9,
//		icon: 'delete',
//		title: 'Delete',
//		description: 'Delete page',
//		visible: _canEdit,
//		action: deleteContent,
//		groups: ['amh.owner-toolbar.scope'],
//		scope: $scope
//	});

	$actions.newAction({// download menu
		id: 'amh.scope.download',
		priority: 8,
		icon: 'cloud_download',
		title: 'Download',
		description: 'Download page',
		action: function(){
			ctrl.downloadModel();
		},
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	
	$actions.newAction({// upload menu
		id: 'amh.scope.upload',
		priority: 7,
		icon: 'cloud_upload',
		title: 'Upload',
		description: 'Upload a page',
		action: function(){
			ctrl.uploadModel();
		},
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});	
	
	
	
	
	
	
	
	
	
	//-----------------------------------InMemory-----------------------------------------
	
	/**
	 * Check if there is a model
	 */
	this.hasModel = function(){
		return true;
	};
	
	/**
	 * Loads meta data of the model to edit
	 */
	this.loadModelMeta = function(){
		return {
			'title': 'No title',
			'description': 'No description',
			'mime_type': '???',
		};
	};
	
	/**
	 * Load a model to edit
	 */
	this.loadModel = function(){
		return {};
	};
	
	/**
	 * Creates new model
	 */
	this.createModel = function(){
		//
	};
	
	/**
	 * Save the model
	 */
	this.saveModel = function(){
		// 
	};
	
	/**
	 * Delete current model
	 */
	this.deleteModel = function(){
		//
	};
	
	
	//-----------------------------------CMS-----------------------------------------
//
//	/*
//	 * Returns name of the contents to load. If use define a name, the actual
//	 * value is returned. In the other case, name of the home content with
//	 * language is returned.
//	 * 
//	 * default language is en
//	 */
//	function contentName() {
//		if ($routeParams.name) {
//			return $routeParams.name;
//		}
//		$scope.app.config.local = $scope.app.config.local || {};
//		return $scope.app.key + '-' + ($routeParams.language ||
//				$scope.app.setting.local ||
//				$scope.app.config.local.language ||
//		'en');
//	}
	

//	/*
//	 * Redirect to the given content
//	 */
//	function _gotoContent(content) {
//		if (contentName() === content.name) {
//			$scope.error = null;
//			$scope.content = content;
//			return loadContentValue();
//		}
//		return $navigator.openPage('content/' + content.name);
//	}
	

//	/*
//	 * Check if possible to create
//	 */
//	function _canCreate() {
//		return $scope.app.user.owner;
//	}
//
//	/*
//	 * Check if possible to edit
//	 */
//	function _canEdit() {
//		var user = $scope.app.user;
//		return (user.core_owner || user.tenant_owner);
//	}
//
//	/*
//	 * Check if it is application page
//	 */
//	function isContentPage() {
//		return $scope.type === 'weburger' || $scope.type === 'unknown';
//	}
//
//	function loadContentValue() {
//		// Check mime type
//		var mimeType = $scope.content.mime_type || 'application/stream';
//		if (mimeType.match(/.*(ogg|mp3|mpeg).*/i)) {
//			$scope.type = 'audio';
//		} else if (mimeType.match(/.*(video).*/i)) {
//			$scope.type = 'video';
//		} else if (mimeType.match(/.*(image).*/i)) {
//			$scope.type = 'image';
//		} else if (mimeType.match(/.*(weburger).*/i)) {
//			$scope.type = 'weburger';
//		} else {
//			$scope.type = 'unknown';
//		}
//		// Check is main page
//		if (!isContentPage()) {
//			// State machine event: no need to download value
//			stateMachine.valueLoaded();
//			return;
//		}
//		// TODO: if is a web page then
//		return $scope.content.downloadValue()
//		.then(function (model) {
//			if (!angular.isObject(model)) {
//				model = {};
//			}
//			// TODO: Maso, 2018: $wbUtil must delete in next version. Here
//			// it comes for compatibility to previous versions.
//			$scope.model = $wbUtil.clean(model);
//			// TODO: maso, 2018: remove old state handlere $scope.initialLoading
//			// = false;
//			// State machine event
//			stateMachine.valueLoaded();
//		}, function (error) {
//			stateMachine.error(error);
//		});
//	}
	

//	/**
//	 * Load content data
//	 * 
//	 * @memberof AmhContentCtrl
//	 * @param {string}
//	 *            contentName The id or name of the content.
//	 * @return {promiss} to load the content data
//	 */
//	function loadModel(contentName) {
//		return $cms.getContent(contentName) //
//		.then(function (content) {
//			$scope.content = content;
//			// State machine event
//			stateMachine.metaLoaded();
//			return loadContentValue();
//		}, function (error) {
//			stateMachine.error(error);
//		})//
//		.finally(function () {
//			delete $scope.working;
//		});
//	}
	
//
//	/**
//	 * Creates new content
//	 * 
//	 * @memberof AmhContentCtrl
//	 * @return {promiss} to load the content data
//	 */
//	function createModel() {
//		if ($scope.working) {
//			return;
//		}
//
//		var data = {
//				title: 'title',
//				description: 'Discription.',
//				mime_type: MIME_WB,
//				file_name: 'page.wb',
//				name: $scope.content ? uuid4.generate() : contentName()
//		};
//		// TODO: update data by user
//		return $scope.working = $navigator.openDialog({
//			templateUrl: 'views/dialogs/' + ($scope.content ? 'amh-content-new.html' : 'amh-content.html'),
//			config: {
//				data: data
//			}
//		})
//		// Create content
//		.then(function (contentData) {
//			$scope.creatingNewContent = true;
//			return $cms.putContent(contentData);
//		}) //
//		.then(function (newContent) {
//			return newContent.uploadValue({})
//			.then(function () {
//				$scope.creatingNewContent = false;
//				return _gotoContent(newContent);
//			});
//		}, function (error) {
//			stateMachine.error(error);
//		})
//		.finally(function () {
//			delete $scope.working;
//		});
//	}

//
//	/**
//	 * Deletes current contetn
//	 */
//	function deleteContent() {
//		return confirm('Delete this page?')
//		.then(function () {
//			$scope.content.delete()//
//			.then(function () {
//				$navigator.openPage('content');
//			});
//		});
//	}
	

//	/**
//	 * clones current content
//	 */
//	function cloneContent() {
//		if ($scope.working) {
//			return toast($translate.instant('Page is loading, please wait.'));
//		}
//		var name = null;
//		// TODO: update data by user
//		return $scope.working = $navigator.openDialog({
//			templateUrl: 'views/dialogs/amh-content-new.html',
//			config: {
//				data: {
//					title: $scope.content.title,
//					description: $scope.content.description,
//					mime_type: MIME_WB,
//					file_name: contentName() + '-clone.wb',
//					name: uuid4.generate()
//				}
//			}
//
//		// Create content
//		}).then(function (contentData) {
//			$scope.creatingNewContent = true;
//			$scope.working = true;
//			return $cms.putContent(contentData)
//			.then(function (newContent) {
//				return newContent.uploadValue($scope.model)
//				.then(function () {
//					$scope.creatingNewContent = false;
//					return _gotoContent(newContent);
//				});
//			}, function (error) {
//				stateMachine.error(error);
//			});
//		}).finally(function () {
//			delete $scope.working;
//		});
//	}
	
//
//
//	/**
//	 * Save value of the content
//	 * 
//	 * @memberof AmhContentCtrl
//	 * @return {promiss} to save content value
//	 */
//	function saveModel() {
//		if (ctrl.savingModel) {
//			return;
//		}
//		ctrl.savingModel = true;
//		stateMachine.saving();
//		// Maso, 2018: update mime and media type of the content to support
//		// backward compatibility
//		$scope.content.mime_type = 'application/weburger+json';
//		$scope.content.media_type = 'page';
//		return $scope.content.update()
//		.then(function (content) {
//			$scope.content = content;
//			return $scope.content.uploadValue($scope.model); //
//		})
//		.then(function () {
//			ctrl.savingModel = false;
//			toast('Page saved successfully.');
//			stateMachine.saved();
//		}, function (error) {
//			ctrl.savingModel = false;
//			alert('Fail to save page.');
//			stateMachine.handleError(error);
//		});
//	}

});